#define PIN_TRIG 4
#define PIN_ECHO 2

#define PIN_RED 7
#define PIN_GREEN 8
#define PIN_BLUE 10
#define PIN_YELLOW 12

#define GARBAGE -1

short distance = GARBAGE;

void ledsOff() {
  digitalWrite(PIN_RED, LOW);
  digitalWrite(PIN_GREEN, LOW);
  digitalWrite(PIN_BLUE, LOW);
  digitalWrite(PIN_YELLOW, LOW);
}

void ledOn(unsigned short pin) {
  digitalWrite(pin, HIGH);
}

void setup() {
  pinMode(PIN_TRIG, OUTPUT);
  pinMode(PIN_ECHO, INPUT);
  
  pinMode(PIN_RED, OUTPUT);
  pinMode(PIN_GREEN, OUTPUT);
  pinMode(PIN_BLUE, OUTPUT);
  pinMode(PIN_YELLOW, OUTPUT);
  
  Serial.begin(115200);
}

void loop() {
  // Take a reading from the ultrasonic sensor
  digitalWrite(PIN_TRIG, LOW);
  delayMicroseconds(2);
  digitalWrite(PIN_TRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(PIN_TRIG, LOW);

  // Compute the distance in centimeters
  unsigned long reading = pulseIn(PIN_ECHO, HIGH) / 58;
  distance = static_cast<short>(reading < 1000 ? reading : GARBAGE);

  // Turn off all LEDs
  ledsOff();
  
  if(distance != GARBAGE) {
    Serial.print("Distance: ");
    Serial.print(distance);
    Serial.println(" cm");

    // Tiny Distance
    if(0 <= distance && distance < 5) {
      ledOn(PIN_RED);
    }
    // Small Distance
    else if(5 <= distance && distance < 10) {
      ledOn(PIN_GREEN);
    }
    // Medium Distance
    else if(10 <= distance && distance < 15) {
      ledOn(PIN_BLUE);
    }
    // Large Distance
    else if(15 <= distance && distance < 20) {
      ledOn(PIN_YELLOW);
    }
    // Huge Distance
    else {
      ledOn(PIN_RED);
      ledOn(PIN_GREEN);
      ledOn(PIN_BLUE);
      ledOn(PIN_YELLOW);
    }

  }

  delay(30);
}
